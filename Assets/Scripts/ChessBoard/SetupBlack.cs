using UnityEngine;
using System.Collections;
using chess;

public class SetupBlack : MonoBehaviour {

    public BoardRow frontRow;
    public BoardRow backRow;

    public void setupNewGame () {
        setupFront();
        setupBack();
    }

    private void setupFront() {
        for (int i = 0; i < (int)col.TOTAL; i++) {
            GameObject obj = Instantiate(ChessBoard.ChessPieceBlack[(int)piece.PAWN]);
            obj.transform.parent = frontRow.column[i].gameObject.transform;
            obj.transform.localPosition = new Vector3(0, 0.5f, 0);
        }
    }

    private void setupBack() {
        GameObject _rook1 = Instantiate(ChessBoard.ChessPieceBlack[(int)piece.ROOK]);
        _rook1.transform.parent = backRow.column[(int)col.A].gameObject.transform;
        _rook1.transform.localPosition = new Vector3(0, 0.5f, 0);

        GameObject _rook2 = Instantiate(ChessBoard.ChessPieceBlack[(int)piece.ROOK]);
        _rook2.transform.parent = backRow.column[(int)col.H].gameObject.transform;
        _rook2.transform.localPosition = new Vector3(0, 0.5f, 0);

        GameObject _knight1 = Instantiate(ChessBoard.ChessPieceBlack[(int)piece.KNIGHT]);
        _knight1.transform.parent = backRow.column[(int)col.B].gameObject.transform;
        _knight1.transform.localPosition = new Vector3(0, 0.5f, 0);

        GameObject _knight2 = Instantiate(ChessBoard.ChessPieceBlack[(int)piece.KNIGHT]);
        _knight2.transform.parent = backRow.column[(int)col.G].gameObject.transform;
        _knight2.transform.localPosition = new Vector3(0, 0.5f, 0);

        GameObject _bishop1 = Instantiate(ChessBoard.ChessPieceBlack[(int)piece.BISHOP]);
        _bishop1.transform.parent = backRow.column[(int)col.C].gameObject.transform;
        _bishop1.transform.localPosition = new Vector3(0, 0.5f, 0);

        GameObject _bishop2 = Instantiate(ChessBoard.ChessPieceBlack[(int)piece.BISHOP]);
        _bishop2.transform.parent = backRow.column[(int)col.F].gameObject.transform;
        _bishop2.transform.localPosition = new Vector3(0, 0.5f, 0);

        GameObject _king = Instantiate(ChessBoard.ChessPieceBlack[(int)piece.KING]);
        _king.transform.parent = backRow.column[(int)col.E].gameObject.transform;
        _king.transform.localPosition = new Vector3(0, 0.5f, 0);
        ChessBoard.BlackKing = _king.GetComponent<King>();

        GameObject _queen = Instantiate(ChessBoard.ChessPieceBlack[(int)piece.QUEEN]);
        _queen.transform.parent = backRow.column[(int)col.D].gameObject.transform;
        _queen.transform.localPosition = new Vector3(0, 0.5f, 0);
    }
}
