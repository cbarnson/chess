using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Assertions;
using System.Collections;
using System.Collections.Generic;
using chess;

public class ChessBoard : MonoBehaviour {

    public static GameObject Chessboard;
    public static GameObject[] ChessPieceBlack = new GameObject[ChessUtility.NUMBER_OF_PIECES];
    public static GameObject[] ChessPieceWhite = new GameObject[ChessUtility.NUMBER_OF_PIECES];
    public static BoardSpace[,] space = new BoardSpace[ChessUtility.MAX_ROWS, ChessUtility.MAX_COLS];
    public static bool[,] highlight = new bool[ChessUtility.MAX_ROWS, ChessUtility.MAX_COLS];
    public static bool[,] highlightFriendly = new bool[ChessUtility.MAX_ROWS, ChessUtility.MAX_COLS];
    public static Material[] ChessMaterial = new Material[ChessUtility.NUMBER_OF_MATERIALS];

    public BoardRow[] rows;
    
    public static bool[,] WhiteThreaten = new bool[ChessUtility.MAX_ROWS, ChessUtility.MAX_COLS];
    public static bool[,] BlackThreaten = new bool[ChessUtility.MAX_ROWS, ChessUtility.MAX_COLS];
    public static bool WhiteInCheck = false;
    public static bool BlackInCheck = false;

    public static List<ChessPiece> WhitePieces = new List<ChessPiece>();
    public static List<ChessPiece> BlackPieces = new List<ChessPiece>();
    public static King WhiteKing;
    public static King BlackKing;

    private static SetupWhite m_setupWhite;
    private static SetupBlack m_setupBlack;

    public static void setMaterial(int row_, int col_, int matType) {
        space[row_, col_].GetComponent<Renderer>().material = ChessMaterial[matType];
        // if was set to VALID OR ENEMY, mark the index in highlight
        if (matType == (int)mat.HIGHLIGHT_VALID || 
            matType == (int)mat.HIGHLIGHT_ENEMY) {
            highlight[row_, col_] = true;
        } else if (matType == (int)mat.HIGHLIGHT_FRIENDLY) {
            highlightFriendly[row_, col_] = true;
        }
    }

    public static void resetSpaceMaterial() {
        for (int i = 0; i < ChessUtility.MAX_ROWS; i++) {
            for (int j = 0; j < ChessUtility.MAX_COLS; j++) {
                if (highlight[i, j]) {
                    space[i, j].resetMaterial();
                    highlight[i, j] = false;
                }
                if (highlightFriendly[i, j]) {
                    space[i, j].resetMaterial();
                    highlightFriendly[i, j] = false;
                }
            }
        }
    }

    public static void getLocation(BoardSpace sp, ref int r, ref int c) {
        for (int i = 0; i < ChessUtility.MAX_ROWS; i++) {
            for (int j = 0; j < ChessUtility.MAX_COLS; j++) {
                if (sp == space[i, j]) {
                    //Debug.Log("found at row: " + i + " column: " + j);
                    r = i;
                    c = j;
                    return;
                }
            }
        }
    }

    public static int getRow(BoardSpace sp) {
        for (int i = 0; i < ChessUtility.MAX_ROWS; i++) {
            for (int j = 0; j < ChessUtility.MAX_COLS; j++) {
                if (sp == space[i, j]) {
                    return i;
                }
            }
        }
        return -1;
    }

    public static int getCol (BoardSpace sp) {
        for (int i = 0; i < ChessUtility.MAX_ROWS; i++) {
            for (int j = 0; j < ChessUtility.MAX_COLS; j++) {
                if (sp == space[i, j]) {
                    return j;
                }
            }
        }
        return -1;
    }

    public static void newGame() {
        // clear other pieces
        clearBoard();
        // setup the two sides
        m_setupWhite.setupNewGame();
        m_setupBlack.setupNewGame();
        // only enable the appropriate color of pieces
        enableValidChessPieces();
        updateLivePieces();
        setThreatenNoKing();
        setThreatenKing();
    }

    public static void clearBoard() {
        for (int i = 0; i < ChessUtility.MAX_ROWS; i++) {
            for (int j = 0; j < ChessUtility.MAX_COLS; j++) {
                if (space[i, j].transform.childCount > 0) {
                    Destroy(space[i, j].transform.GetChild(0).gameObject);
                }
            }
        }
    }

    public static void disableColliders() {
        for (int i = 0; i < ChessUtility.MAX_ROWS; i++) {
            for (int j = 0; j < ChessUtility.MAX_COLS; j++) {
                space[i, j]._collider.enabled = false;
            }
        }
    }

    public static void enableColliders() {
        for (int i = 0; i < ChessUtility.MAX_ROWS; i++) {
            for (int j = 0; j < ChessUtility.MAX_COLS; j++) {
                space[i, j]._collider.enabled = true;
            }
        }
    }
    // must be called after spaces have been highlighted
    public static void enableValidColliders() {
        for (int i = 0; i < ChessUtility.MAX_ROWS; i++) {
            for (int j = 0; j < ChessUtility.MAX_COLS; j++) {
                // if highlight is active
                if (highlight[i, j]) {
                    // enable the corresponding collider
                    space[i, j]._collider.enabled = true;
                }
            }
        }
    }

    // to be used after a move occurs, before the user hits the END TURN button
    public static void disableChessPieces() {
        Component[] pieces;
        pieces = Chessboard.GetComponentsInChildren(typeof(ChessPiece));
        if (pieces != null) {
            foreach (ChessPiece cp in pieces) {
                cp._collider.enabled = false;
            }
        } else {
            Debug.Log("error, no chesspieces found");
        }
    }

    public static void enableValidChessPieces() {
        Component[] pieces;
        pieces = Chessboard.GetComponentsInChildren(typeof(ChessPiece));
        if (pieces != null) {
            foreach (ChessPiece cp in pieces) {
                if (Player.m_whiteTurn) {
                    // deactivate black, activate white
                    if (!cp.isWhite) { cp._collider.enabled = false; }
                    else { cp._collider.enabled = true; }
                } else {
                    // deactivate white, activate black
                    if (cp.isWhite) { cp._collider.enabled = false; } 
                    else { cp._collider.enabled = true; }
                }
            }
        } else {
            Debug.Log("error, no chesspieces found");
        }
    }

    public static void updateLivePieces() {
        // clear lists
        WhitePieces.Clear();
        BlackPieces.Clear();
        Component[] pieces;
        pieces = Chessboard.GetComponentsInChildren(typeof(ChessPiece));
        if (pieces != null) {
            foreach (ChessPiece cp in pieces) {
                if (cp.isWhite) WhitePieces.Add(cp);
                else BlackPieces.Add(cp);
                // reset these values
                cp.isChecking = false;
            }
        }
        Debug.Log("number of white: " + WhitePieces.Count);
        Debug.Log("number of black: " + BlackPieces.Count);
    }

    public static void clearThreaten () {
        for (int i = 0; i < ChessUtility.MAX_ROWS; i++) {
            for (int j = 0; j < ChessUtility.MAX_COLS; j++) {
                WhiteThreaten[i, j] = false;
                BlackThreaten[i, j] = false;
            }
        }
        WhiteInCheck = false;
        BlackInCheck = false;
    }

    public static void setThreatenKing () {
        foreach (ChessPiece cp in WhitePieces) {
            if (cp.ID == (int)piece.KING) { cp.getValidMoves(cp.transform.parent); }
        }
        foreach (ChessPiece cp in BlackPieces) {
            if (cp.ID == (int)piece.KING) { cp.getValidMoves(cp.transform.parent); }
        }
    }

    // for now, just set the threaten spaces for both sides
    public static void setThreatenNoKing() {
        foreach (ChessPiece cp in WhitePieces) {
            if (cp.ID != (int)piece.KING) { cp.getValidMoves(cp.transform.parent); }
        }
        foreach (ChessPiece cp in BlackPieces) {
            if (cp.ID != (int)piece.KING) { cp.getValidMoves(cp.transform.parent); }
        }
    }

    // AWAKE
    void Awake () {
        loadBlackPieces();
        loadWhitePieces();
        loadSpaces();
        loadMaterials();
        m_setupWhite = gameObject.GetComponent<SetupWhite>();
        m_setupBlack = gameObject.GetComponent<SetupBlack>();
        Chessboard = gameObject;
    }

    void Start () {
        // disable the END TURN button
        TurnCanvas.m_button.interactable = false;
    }

    void loadSpaces() {
        for (int i = 0; i < ChessUtility.MAX_ROWS; i++) {
            for (int j = 0; j < ChessUtility.MAX_COLS; j++) {
                space[i, j] = rows[i].column[j];
                highlight[i, j] = false;
                highlightFriendly[i, j] = false;
            }
        }
    }

    void loadBlackPieces() {
        for (int i = 0; i < ChessUtility.NUMBER_OF_PIECES; i++) {
            ChessPieceBlack[i] = Resources.Load(ChessUtility.BlackPieceFile[i], typeof(GameObject)) as GameObject;
        }
    }

    void loadWhitePieces() {
        for (int i = 0; i < ChessUtility.NUMBER_OF_PIECES; i++) {
            ChessPieceWhite[i] = Resources.Load(ChessUtility.WhitePieceFile[i], typeof(GameObject)) as GameObject;
        }
    }

    void loadMaterials() {
        for (int i = 0; i < ChessUtility.NUMBER_OF_MATERIALS; i++) {
            ChessMaterial[i] = Resources.Load(ChessUtility.MaterialFile[i], typeof(Material)) as Material;
        }
    }

}

