using UnityEngine;
using System.Collections;

public class NewGame : MonoBehaviour {

    public void startNewGame() {
        ChessBoard.newGame();
    }
}
