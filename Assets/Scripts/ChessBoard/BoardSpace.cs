using UnityEngine;
using UnityEngine.Assertions;
using System.Collections;
using chess;

public class BoardSpace : MonoBehaviour {

    public bool isWhite;
    public char column;  // A to H

    [HideInInspector]
    public GameObject occupied;
    [HideInInspector]
    public Collider _collider;


    void Awake () {
        _collider = GetComponent<Collider>();
        _collider.enabled = false;
    }

    void OnTransformChildrenChanged() {
        if (gameObject.transform.childCount < 1) {
            occupied = null;
        } else {
            occupied = gameObject.transform.GetChild(0).gameObject;
        }
    }

    public void resetMaterial() {
        if (isWhite) {
            GetComponent<Renderer>().material = ChessBoard.ChessMaterial[(int)mat.WHITE];
        } else {
            GetComponent<Renderer>().material = ChessBoard.ChessMaterial[(int)mat.BLACK];
        }
    }

    public bool hasChessPiece() {
        if (transform.childCount > 0) return true;
        return false;
    }

    public bool hasFriendlyPiece() {
        Assert.IsNotNull(Player.m_holding);
        if (transform.childCount > 0) {
            // hasWhite true if containing a white piece
            bool hasWhite = gameObject.GetComponentInChildren<ChessPiece>().isWhite;
            if (hasWhite && Player.m_holding.GetComponent<ChessPiece>().isWhite) {
                return true; // both white
            } else if (!hasWhite && !Player.m_holding.GetComponent<ChessPiece>().isWhite) {
                return true; // both black
            }
        }
        // no child or not friendly
        return false;
    }

    public bool hasFriendly(bool _white) {
        if (transform.childCount > 0) {
            bool hasWhite = gameObject.GetComponentInChildren<ChessPiece>().isWhite;
            if (hasWhite && _white) {
                return true; // both white
            } else if (!hasWhite && !_white) {
                return true; // both black
            }
        }
        // no child or not friendly
        return false;
    }

    public bool hasPawnPiece() {
        if (transform.childCount > 0) {
            Pawn piece = transform.GetChild(0).gameObject.GetComponent<Pawn>();
            if (piece != null) {
                return true;
            }
        }
        return false;
    }

    public bool hasKingPiece () {
        if (transform.childCount > 0) {
            King piece = transform.GetChild(0).gameObject.GetComponent<King>();
            if (piece != null) {
                return true;
            }
        }
        return false;
    }

}
