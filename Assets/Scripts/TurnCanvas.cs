using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using chess;

public class TurnCanvas : MonoBehaviour {

    public static Button m_button;

    public Fade white;
    public Fade black;
    private bool _whiteTurn = true;

    void Awake () {
        white.gameObject.GetComponent<CanvasGroup>().alpha = 1.0f;
        black.gameObject.GetComponent<CanvasGroup>().alpha = 0.0f;
        m_button = gameObject.GetComponentInChildren<Button>();
    }

    public void switchTurn() {
        // change turn flag
        Player.m_whiteTurn = !Player.m_whiteTurn;
        // update chess piece colliders
        ChessBoard.enableValidChessPieces();
        // turn label update
        if (Player.m_whiteTurn) {
            white.FadeMeIn();
            black.FadeMeOut();
        } else {
            white.FadeMeOut();
            black.FadeMeIn();
        }
        // updates WhitePieces and BlackPieces remaining in the game
        ChessBoard.updateLivePieces();
        // clear the bool arrays to all false
        ChessBoard.clearThreaten();
        // set the new values
        ChessBoard.setThreatenNoKing();
        // after switching, make the button disabled again
        m_button.interactable = false;

        if (isCheckmateWhite()) {
            Debug.Log("checkmate white");
        }

        if (isCheckmateBlack()) {
            Debug.Log("checkmate black");
        }

        ChessBoard.setThreatenKing();

    }
    
    // for now, only bother with single check
    private bool isCheckmateWhite() {
        if (Player.m_whiteTurn && ChessBoard.WhiteInCheck) {
            List<ChessPiece> checking = new List<ChessPiece>();
            for (int i = 0; i < ChessBoard.BlackPieces.Count; i++) { // build a list of all checking pieces
                if (ChessBoard.BlackPieces[i].isChecking) {
                    checking.Add(ChessBoard.BlackPieces[i]);
                }
            }
            Debug.Log("number of checking black pieces: " + checking.Count);
            for (int i = 0; i < checking.Count; i++) {
                // get location of the checking piece
                int u = 0; int v = 0;
                ChessBoard.getLocation(checking[i].GetComponentInParent<BoardSpace>(), ref u, ref v);
                if (isAdjacent(ChessBoard.WhiteKing, checking[i])) {
                    // is this location threatened by white
                    if (ChessBoard.WhiteThreaten[u, v]) {
                        // not checkmate
                        return false;
                    } else {
                        // can king take out the checking piece?
                        // is the enemy piece's space covered by another enemy piece
                        if (ChessBoard.BlackThreaten[u, v]) {
                            // king cannot take the piece, can king move away?
                            if (kingMayFlee(ChessBoard.WhiteKing)) {
                                // not checkmate, king can move away
                                return false;
                            } else {
                                // king has no valid move, checkmate
                                return true;
                            }
                        } else {
                            // not checkmate, king can take it out
                            return false;
                        }
                    }
                } else {
                    // checking piece is not adjacent
                    // is this location threatened by white?
                    if (ChessBoard.WhiteThreaten[u, v]) {
                        // not checkmate, white may take the checking piece
                        return false;
                    } else {
                        // can the king move away
                        if (kingMayFlee(ChessBoard.WhiteKing)) {
                            return false;
                        } else {
                            return true; // checkmate
                        }
                    }
                }
            }
        }
        return false;
    }

    private bool isCheckmateBlack () {
        if (!Player.m_whiteTurn && ChessBoard.BlackInCheck) {
            List<ChessPiece> checking = new List<ChessPiece>();
            for (int i = 0; i < ChessBoard.WhitePieces.Count; i++) { // build a list of all checking pieces
                if (ChessBoard.WhitePieces[i].isChecking) {
                    checking.Add(ChessBoard.WhitePieces[i]);
                }
            }
            Debug.Log("number of checking white pieces: " + checking.Count);
            for (int i = 0; i < checking.Count; i++) {
                int u = 0; int v = 0;
                ChessBoard.getLocation(checking[i].GetComponentInParent<BoardSpace>(), ref u, ref v);
                if (isAdjacent(ChessBoard.BlackKing, checking[i])) {
                    // is this location threatened by black
                    if (ChessBoard.BlackThreaten[u, v]) {
                        // not checkmate
                        return false;
                    } else {
                        // can king take out the checking piece?
                        // is the enemy piece's space covered by another enemy piece
                        if (ChessBoard.WhiteThreaten[u, v]) {
                            // king cannot take the piece, can king move away?
                            if (kingMayFlee(ChessBoard.BlackKing)) {
                                // not checkmate, king can move away
                                return false;
                            } else {
                                // king has no valid move, checkmate
                                return true;
                            }
                        } else {
                            // not checkmate, king can take it out
                            return false;
                        }
                    }
                } else {
                    // not adjacent
                    // is this location threatened by black?
                    if (ChessBoard.BlackThreaten[u, v]) {
                        // not checkmate, black may take the checking piece
                        return false;
                    } else {
                        // can the king move away
                        if (kingMayFlee(ChessBoard.BlackKing)) {
                            return false;
                        } else {
                            return true; // checkmate
                        }
                    }
                }
            }
        }
        return false;
    }

    private bool kingMayFlee(King king) {

        // condition for king can flee
        int r = 0; int c = 0;
        ChessBoard.getLocation(king.GetComponentInParent<BoardSpace>(), ref r, ref c);
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                // dont check the kings current spot
                if (i == 0 && j == 0) continue;
                // check if inbound
                if (ChessUtility.inBound(r + i, c + j)) {
                    // is the space not threatened by the opposite team
                    if (king.isWhite) {
                        // white king
                        if (!ChessBoard.BlackThreaten[r + i, c + j]) {
                            // black does not threaten the space
                            if (!ChessBoard.space[r + i, c + j].hasChessPiece() || (ChessBoard.space[r + i, c + j].hasChessPiece() && !ChessBoard.space[r + i, c + j].hasFriendly(king.isWhite))) {
                                // space has no piece, or the space has an enemy piece
                                return true;
                            }
                        }
                    } else {
                        // black king
                        if (!ChessBoard.WhiteThreaten[r + i, c + j]) {
                            // black does not threaten the space
                            if (!ChessBoard.space[r + i, c + j].hasChessPiece() || (ChessBoard.space[r + i, c + j].hasChessPiece() && !ChessBoard.space[r + i, c + j].hasFriendly(king.isWhite))) {
                                // space has no piece, or the space has an enemy piece
                                return true;
                            }
                        }
                    }
                }
            }
        }
        // did not find a valid space
        return false;
    }

    private bool isAdjacent(King king, ChessPiece checking) {

        int row_k = 0; int col_k = 0;
        int row_c = 0; int col_c = 0;
        ChessBoard.getLocation(king.GetComponentInParent<BoardSpace>(), ref row_k, ref col_k);
        ChessBoard.getLocation(checking.GetComponentInParent<BoardSpace>(), ref row_c, ref col_c);

        // to be implemented
        if (Mathf.Abs(row_k - row_c) < 2 && Mathf.Abs(col_k - col_c) < 2) {
            return true;
        }

        return false;
    }

}
