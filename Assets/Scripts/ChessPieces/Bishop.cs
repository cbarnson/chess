using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using chess;

public class Bishop : ChessPiece {

    private List<location> moves;

    protected override void Awake() {
        base.Awake();
        ID = (int)piece.BISHOP;
        nameID = "bishop";
    }

    public override void showValidMoves (Transform start) {
        bishopMoves(start);
        highlightMoves();
    }

    public override void getValidMoves (Transform start) {
        if (isWhite) {
            bishopMoves(start);
            for (int i = 0; i < moves.Count; i++) {
                ChessBoard.WhiteThreaten[moves[i].R, moves[i].C] = true;
                if (ChessBoard.space[moves[i].R, moves[i].C].hasKingPiece() && 
                    !ChessBoard.space[moves[i].R, moves[i].C].hasFriendly(this.isWhite)) {
                    this.isChecking = true;
                    ChessBoard.BlackInCheck = true;
                }
            }
        } else {
            bishopMoves(start);
            for (int i = 0; i < moves.Count; i++) {
                ChessBoard.BlackThreaten[moves[i].R, moves[i].C] = true;
                if (ChessBoard.space[moves[i].R, moves[i].C].hasKingPiece() &&
                    !ChessBoard.space[moves[i].R, moves[i].C].hasFriendly(this.isWhite)) {
                    this.isChecking = true;
                    ChessBoard.WhiteInCheck = true;
                }
            }
        }
    }

    private void highlightMoves () {
        for (int i = 0; i < moves.Count; i++) {
            // check for friendly piece, continue if found
            if (ChessBoard.space[moves[i].R, moves[i].C].hasFriendlyPiece()) {
                ChessBoard.setMaterial(moves[i].R, moves[i].C, (int)mat.HIGHLIGHT_FRIENDLY);
                continue;
            }
            // if space is occupied, we know it is not friendly, so it is enemy
            if (ChessBoard.space[moves[i].R, moves[i].C].hasChessPiece()) {
                ChessBoard.setMaterial(moves[i].R, moves[i].C, (int)mat.HIGHLIGHT_ENEMY);
            } else {
                // else is a valid move space
                ChessBoard.setMaterial(moves[i].R, moves[i].C, (int)mat.HIGHLIGHT_VALID);
            }
        }
    }

    private void bishopMoves (Transform start) {
        int row_ = 0;
        int column_ = 0;
        // NOTE: these indices are 0-based
        ChessBoard.getLocation(start.GetComponent<BoardSpace>(), ref row_, ref column_);
        moves = new List<location>();

        bool[] direction = new bool[4];
        for (int i = 0; i < direction.Length; i++) { direction[i] = true; }

        for (int i = 1; i < ChessUtility.MAX_COLS; i++) {
            if (ChessUtility.inBound(row_ + i, column_ + i) && direction[0]) {
                moves.Add(new location() { R = row_ + i, C = column_ + i });
                if (ChessBoard.space[row_ + i, column_ + i].hasChessPiece()) direction[0] = false;
            }
            if (ChessUtility.inBound(row_ - i, column_ - i) && direction[1]) {
                moves.Add(new location() { R = row_ - i, C = column_ - i });
                if (ChessBoard.space[row_ - i, column_ - i].hasChessPiece()) direction[1] = false;
            }
            if (ChessUtility.inBound(row_ + i, column_ - i) && direction[2]) {
                moves.Add(new location() { R = row_ + i, C = column_ - i });
                if (ChessBoard.space[row_ + i, column_ - i].hasChessPiece()) direction[2] = false;
            }
            if (ChessUtility.inBound(row_ - i, column_ + i) && direction[3]) {
                moves.Add(new location() { R = row_ - i, C = column_ + i });
                if (ChessBoard.space[row_ - i, column_ + i].hasChessPiece()) direction[3] = false;
            }
        }
    }
}
