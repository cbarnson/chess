using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using System;
using chess;

public class ChessPiece : MonoBehaviour {

    public bool isWhite;
    public int ID;
    public string nameID;
    public bool isChecking = false;

    public Collider _collider;

    protected virtual void Awake () {
        _collider = GetComponent<Collider>();
    }

    public virtual void showValidMoves(Transform start) {
        Debug.Log("show valid moves base function");
    }

    public virtual void getValidMoves(Transform start) {
        // get moves, mark them in bool 2D array in ChessBoard
    }

}
