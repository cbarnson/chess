using UnityEngine;
using System;
using System.Collections;
using chess;
using System.Collections.Generic;

public class Pawn : ChessPiece {

    [HideInInspector]
    public bool firstMoveComplete = false;
    private List<location> moves;
    private List<location> enpassant;
    public bool canTakeEnpassant = false;

    protected override void Awake() {
        base.Awake();
        ID = (int)piece.PAWN;
        nameID = "pawn";
        enpassant = new List<location>();
    }

    // returns true if location of this pawn matches the enpassant take location
    public bool getEnpassant() {
        int r_ = 0; int c_ = 0;
        for (int i = 0; i < enpassant.Count; i++) {
            ChessBoard.getLocation(transform.parent.GetComponent<BoardSpace>(), ref r_, ref c_);
            if (r_ == enpassant[i].R && c_ == enpassant[i].C) {
                return true;
            }
        }
        return false;
    }

    // call through the double-stepping pawn - allows THIS pawn the enpassant maneuver in the param location  
    public void setEnpassant(int r, int c) {
        // set flag to signal the maneuver is allowed
        //Debug.Log("inside setEnpassant");
        canTakeEnpassant = true;
        enpassant.Add(new location() { R = r, C = c });
    }

    public void disableEnpassant() {
        //Debug.Log("inside disableEnpassant");
        canTakeEnpassant = false;
        enpassant = new List<location>();
    }

    public override void showValidMoves (Transform start) {
        if (isWhite) {
            whitePawnMoves(start);
        } else {
            blackPawnMoves(start);
        }
        highlightMoves();
    }

    public override void getValidMoves (Transform start) {
        if (isWhite) {
            whitePawnMoves(start);
            for (int i = 0; i < moves.Count; i++) {
                ChessBoard.WhiteThreaten[moves[i].R, moves[i].C] = true;
                if (ChessBoard.space[moves[i].R, moves[i].C].hasKingPiece() &&
                    !ChessBoard.space[moves[i].R, moves[i].C].hasFriendly(this.isWhite)) {
                    this.isChecking = true;
                    ChessBoard.BlackInCheck = true;
                }
            }
        } else {
            blackPawnMoves(start);
            for (int i = 0; i < moves.Count; i++) {
                ChessBoard.BlackThreaten[moves[i].R, moves[i].C] = true;
                if (ChessBoard.space[moves[i].R, moves[i].C].hasKingPiece() &&
                    !ChessBoard.space[moves[i].R, moves[i].C].hasFriendly(this.isWhite)) {
                    this.isChecking = true;
                    ChessBoard.WhiteInCheck = true;
                }
            }
        }
    }

    private void highlightMoves() {

        // enpassant
        if (canTakeEnpassant) {
            for (int i = 0; i < enpassant.Count; i++) {
                ChessBoard.setMaterial(enpassant[i].R, enpassant[i].C, (int)mat.HIGHLIGHT_ENEMY);
            }
        }

        for (int i = 0; i < moves.Count; i++) {
            // check for friendly piece, continue if found
            if (ChessBoard.space[moves[i].R, moves[i].C].hasFriendlyPiece()) {
                ChessBoard.setMaterial(moves[i].R, moves[i].C, (int)mat.HIGHLIGHT_FRIENDLY);
                continue;
            }
            // if space is occupied, we know it is not friendly, so it is enemy
            if (ChessBoard.space[moves[i].R, moves[i].C].hasChessPiece()) {
                ChessBoard.setMaterial(moves[i].R, moves[i].C, (int)mat.HIGHLIGHT_ENEMY);
            } else {
                // else is a valid move space
                ChessBoard.setMaterial(moves[i].R, moves[i].C, (int)mat.HIGHLIGHT_VALID);
            }
        }
    }

    private void whitePawnMoves(Transform start) {
        int row_ = 0;
        int column_ = 0;
        // NOTE: these indices are 0-based
        ChessBoard.getLocation(start.GetComponent<BoardSpace>(), ref row_, ref column_);
        moves = new List<location>();
        // pawn move forward 1 space
        if (ChessUtility.inBound(row_ + 1, column_) && !ChessBoard.space[row_ + 1, column_].hasChessPiece())
            moves.Add(new location() { R = row_ + 1, C = column_ });
        
        // double step
        if (!firstMoveComplete && ChessUtility.inBound(row_ + 2, column_) && !ChessBoard.space[row_ + 2, column_].hasChessPiece()) {
            moves.Add(new location() { R = row_ + 2, C = column_ });
        }

        // check for black piece in forward-diagonal location
        if (ChessUtility.inBound(row_ + 1, column_ + 1)) {
            // if space has a chesspiece, and that chesspiece is black, add the move
            if (ChessBoard.space[row_ + 1, column_ + 1].hasChessPiece() &&
                !ChessBoard.space[row_ + 1, column_ + 1].transform.GetChild(0).gameObject.GetComponent<ChessPiece>().isWhite) {
                moves.Add(new location() { R = row_ + 1, C = column_ + 1 });
            }
        }
        if (ChessUtility.inBound(row_ + 1, column_ - 1)) {
            // if space has a chesspiece, and that chesspiece is black, add the move
            if (ChessBoard.space[row_ + 1, column_ - 1].hasChessPiece() &&
                !ChessBoard.space[row_ + 1, column_ - 1].transform.GetChild(0).gameObject.GetComponent<ChessPiece>().isWhite) {
                moves.Add(new location() { R = row_ + 1, C = column_ - 1 });
            }
        }

        
    }

    private void blackPawnMoves(Transform start) {
        int row_ = 0;
        int column_ = 0;
        // NOTE: these indices are 0-based
        ChessBoard.getLocation(start.GetComponent<BoardSpace>(), ref row_, ref column_);
        moves = new List<location>();
        // pawn move forward 1 space
        if (ChessUtility.inBound(row_ - 1, column_) && !ChessBoard.space[row_ - 1, column_].hasChessPiece())
            moves.Add(new location() { R = row_ - 1, C = column_ });

        // double step
        if (!firstMoveComplete && ChessUtility.inBound(row_ - 2, column_) && !ChessBoard.space[row_ - 2, column_].hasChessPiece()) {
            moves.Add(new location() { R = row_ - 2, C = column_ });
        }   

        // check for black piece in forward-diagonal location
        if (ChessUtility.inBound(row_ - 1, column_ + 1)) {
            // if space has a chesspiece, and that chesspiece is black, add the move
            if (ChessBoard.space[row_ - 1, column_ + 1].hasChessPiece() &&
                ChessBoard.space[row_ - 1, column_ + 1].transform.GetChild(0).gameObject.GetComponent<ChessPiece>().isWhite) {
                moves.Add(new location() { R = row_ - 1, C = column_ + 1 });
            }
        }
        if (ChessUtility.inBound(row_ - 1, column_ - 1)) {
            // if space has a chesspiece, and that chesspiece is black, add the move
            if (ChessBoard.space[row_ - 1, column_ - 1].hasChessPiece() &&
                ChessBoard.space[row_ - 1, column_ - 1].transform.GetChild(0).gameObject.GetComponent<ChessPiece>().isWhite) {
                moves.Add(new location() { R = row_ - 1, C = column_ - 1 });
            }
        }

    }

}
