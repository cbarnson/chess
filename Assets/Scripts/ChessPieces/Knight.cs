using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using chess;

public class Knight : ChessPiece {

    private List<location> moves;

    protected override void Awake() {
        base.Awake();
        ID = (int)piece.KNIGHT;
        nameID = "knight";
    }

    public override void showValidMoves (Transform start) {
        knightMoves(start);
        highlightMoves();
    }

    public override void getValidMoves (Transform start) {
        if (isWhite) {
            knightMoves(start);
            for (int i = 0; i < moves.Count; i++) {
                ChessBoard.WhiteThreaten[moves[i].R, moves[i].C] = true;
                if (ChessBoard.space[moves[i].R, moves[i].C].hasKingPiece() &&
                    !ChessBoard.space[moves[i].R, moves[i].C].hasFriendly(this.isWhite)) {
                    this.isChecking = true;
                    ChessBoard.BlackInCheck = true;
                }
            }
        } else {
            knightMoves(start);
            for (int i = 0; i < moves.Count; i++) {
                ChessBoard.BlackThreaten[moves[i].R, moves[i].C] = true;
                if (ChessBoard.space[moves[i].R, moves[i].C].hasKingPiece() &&
                    !ChessBoard.space[moves[i].R, moves[i].C].hasFriendly(this.isWhite)) {
                    this.isChecking = true;
                    ChessBoard.WhiteInCheck = true;
                }
            }
        }
    }

    private void highlightMoves () {
        for (int i = 0; i < moves.Count; i++) {
            // check for friendly piece, continue if found
            if (ChessBoard.space[moves[i].R, moves[i].C].hasFriendlyPiece()) {
                ChessBoard.setMaterial(moves[i].R, moves[i].C, (int)mat.HIGHLIGHT_FRIENDLY);
                continue;
            }
            // if space is occupied, we know it is not friendly, so it is enemy
            if (ChessBoard.space[moves[i].R, moves[i].C].hasChessPiece()) {
                ChessBoard.setMaterial(moves[i].R, moves[i].C, (int)mat.HIGHLIGHT_ENEMY);
            } else {
                // else is a valid move space
                ChessBoard.setMaterial(moves[i].R, moves[i].C, (int)mat.HIGHLIGHT_VALID);
            }
        }
    }

    private void knightMoves (Transform start) {
        int row_ = 0;
        int column_ = 0;
        // NOTE: these indices are 0-based
        ChessBoard.getLocation(start.GetComponent<BoardSpace>(), ref row_, ref column_);
        moves = new List<location>();
        if (ChessUtility.inBound(row_ + 2, column_ + 1)) moves.Add(new location() { R = row_ + 2, C = column_ + 1 });
        if (ChessUtility.inBound(row_ + 1, column_ + 2)) moves.Add(new location() { R = row_ + 1, C = column_ + 2 });
        if (ChessUtility.inBound(row_ - 1, column_ + 2)) moves.Add(new location() { R = row_ - 1, C = column_ + 2 });
        if (ChessUtility.inBound(row_ - 2, column_ + 1)) moves.Add(new location() { R = row_ - 2, C = column_ + 1 });
        if (ChessUtility.inBound(row_ - 2, column_ - 1)) moves.Add(new location() { R = row_ - 2, C = column_ - 1 });
        if (ChessUtility.inBound(row_ - 1, column_ - 2)) moves.Add(new location() { R = row_ - 1, C = column_ - 2 });
        if (ChessUtility.inBound(row_ + 1, column_ - 2)) moves.Add(new location() { R = row_ + 1, C = column_ - 2 });
        if (ChessUtility.inBound(row_ + 2, column_ - 1)) moves.Add(new location() { R = row_ + 2, C = column_ - 1 });
    }

}
