using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class Player : MonoBehaviour {

    public static Camera m_camera;
    public static bool m_whiteTurn;

    public static GameObject m_holding;
    public static Transform m_transform;

    void Awake () {
        m_camera = gameObject.GetComponentInChildren<Camera>();
        m_whiteTurn = true;
        m_transform = gameObject.transform;
        Input.multiTouchEnabled = false;
    }
}
