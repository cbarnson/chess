using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;

[RequireComponent (typeof (BoardSpace))]
public class DropHandler : MonoBehaviour {//, IDropHandler {

    //public static Transform m_potentialDropParent;

    public GameObject isOccupied {
        get {
            // there is a child
            if (transform.childCount > 0) {
                //int r = gameObject.GetComponentInParent<BoardRow>().row;
                //Debug.Log("child count > 0 - " + gameObject.name + " in row " + r);
                return transform.GetChild(0).gameObject;
            }
            // there is not a child
            return null;
        }
    }

    //public void OnDrop (PointerEventData eventData) {
        //Debug.Log("ON DROP");

        //if (isOccupied) return;
        //Debug.Log("is not occupied, setting new parent");
        //Player.m_holding.transform.parent = gameObject.transform;
        //ExecuteEvents.ExecuteHierarchy<IHasChanged>(gameObject, null, (x, y) => x.HasChanged());

        //if (m_potentialDropParent) {
        //    Player.m_holding.transform.parent = m_potentialDropParent;
        //    ExecuteEvents.ExecuteHierarchy<IHasChanged>(gameObject, null, (x, y) => x.HasChanged());
        //}
    //}

    void OnTriggerEnter(Collider other) {

        // if occupied already, do nothing
        //if (isOccupied) return;
        if (transform.childCount > 0) return;
        // set parent to this object
        other.gameObject.transform.parent = transform;

        //if (!isOccupied) {
        //    // valid space for next drop, set this space to potential drop
        //    Debug.Log("valid space for drop");
        //    m_potentialDropParent = gameObject.transform;
        //}
        //Debug.Log("invalid space for drop");
        //m_potentialDropParent = null;
    }

}
