using UnityEngine;
using System.Collections;

public class Pivot : MonoBehaviour {

    public static GameObject m_pivotObject;
    public static Pivot m_pivot;
    public float rotateRate = 100.0f;

    void Awake () {
        m_pivotObject = gameObject;
        m_pivot = this;
    }

    void FixedUpdate () {
        if (Player.m_whiteTurn) {
            float angle = Mathf.MoveTowardsAngle(m_pivot.transform.eulerAngles.y, 0.0f, rotateRate * Time.deltaTime);
            m_pivot.transform.eulerAngles = new Vector3(0, angle, 0);
        } else {
            float angle = Mathf.MoveTowardsAngle(m_pivot.transform.eulerAngles.y, 180.0f, rotateRate * Time.deltaTime);
            m_pivot.transform.eulerAngles = new Vector3(0, angle, 0);
        }
    }
}
