using UnityEngine;
using UnityEditor;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using System.Collections;
using System;
using chess;

public class DragHandler : MonoBehaviour, IBeginDragHandler, IInitializePotentialDragHandler, IDragHandler, IEndDragHandler {

    public static float liftRate = 5.0f;
    public static float dropRate = 10.0f;
    public static float liftDistance = 0.5f;

    private float _originDistanceAboveBoard;
    private float _maxDistanceAboveBoard;
    private float _distanceToScreen;

    private Vector3 _startPosition;
    private Transform _startParent;

    IEnumerator liftObject() {
        while (transform.position.y < _maxDistanceAboveBoard) {
            float _movingDistanceAboveBoard = 
                Mathf.MoveTowards(transform.position.y, _maxDistanceAboveBoard, liftRate * Time.deltaTime);
            transform.position = new Vector3(transform.position.x, _movingDistanceAboveBoard, transform.position.z);
            yield return null;
        }
        yield return null;
    }

    IEnumerator dropObject () {
        while (transform.position.y > _originDistanceAboveBoard) {
            float _movingDistanceAboveBoard = 
                Mathf.MoveTowards(transform.position.y, _originDistanceAboveBoard, liftRate * Time.deltaTime);
            transform.position = new Vector3(transform.position.x, _movingDistanceAboveBoard, transform.position.z);
            yield return null;
        }
        yield return null;
    }

    public void OnInitializePotentialDrag (PointerEventData eventData) {
        _originDistanceAboveBoard = transform.position.y;
        _maxDistanceAboveBoard = _originDistanceAboveBoard + liftDistance;
    }

    public void OnBeginDrag (PointerEventData eventData) {
        _startPosition = transform.position;
        _startParent = transform.parent;
        Player.m_holding = eventData.pointerDrag;
        ChessPiece thisPiece = gameObject.GetComponent<ChessPiece>();
        thisPiece.showValidMoves(_startParent);
        _startParent.gameObject.GetComponent<Collider>().enabled = true;
        ChessBoard.enableValidColliders();
        StartCoroutine(liftObject());
    }

    public void OnDrag (PointerEventData eventData) {
        _distanceToScreen = eventData.pressEventCamera.WorldToScreenPoint(gameObject.transform.position).z;
        Vector3 moveTo = eventData.pressEventCamera.ScreenToWorldPoint(
            new Vector3(eventData.position.x, eventData.position.y, _distanceToScreen));
        transform.position = new Vector3(moveTo.x, transform.position.y, moveTo.z);
    }

    public void OnEndDrag (PointerEventData eventData) {
        ChessBoard.resetSpaceMaterial();
        ChessBoard.disableColliders();
        // MOVE DID NOT OCCUR
        if (noMoveOccurred()) { return; }
        // REQUEST FOR MOVE VALIDATION - handles move attempt OUT of check
        if (isIllegalMoveFromCheck()) { return; }
        // REQUEST FOR MOVE VALIDATION - ensures move does not put the player INTO check
        if (isIllegalMoveToCheck()) { return; }
        
        // proceed with normal move
        if (transform.parent.childCount > 1) { killEnemyPiece(); }
        transform.localPosition = new Vector3(0, transform.position.y, 0);
        checkPawnSpecialCase();
        StartCoroutine(dropObject());
        TurnCanvas.m_button.interactable = true;
        ChessBoard.disableChessPieces();
        Player.m_holding = null;
    }

    private bool noMoveOccurred () {
        if (transform.parent == _startParent) {
            transform.position = new Vector3(_startPosition.x, transform.position.y, _startPosition.z);
            StartCoroutine(dropObject());
            Player.m_holding = null;
            return true;
        }
        return false;
    }

    // REQUEST FOR MOVE VALIDATION - ensures move does not put the player INTO check
    private bool isIllegalMoveToCheck() {
        if (transform.parent.childCount > 1) {
            killAndStore();
            ChessBoard.updateLivePieces();
            ChessBoard.clearThreaten();
            ChessBoard.setThreatenNoKing();
            ChessBoard.setThreatenKing();
            if (Player.m_whiteTurn && ChessBoard.WhiteInCheck || !Player.m_whiteTurn && ChessBoard.BlackInCheck) {
                Debug.Log("illegal take - move puts player in check");
                Undo.PerformUndo();
                transform.parent = _startParent;
                transform.position = new Vector3(_startPosition.x, transform.position.y, _startPosition.z);
                StartCoroutine(dropObject());
                Player.m_holding = null;
                return true;
            }
        } else {
            ChessBoard.updateLivePieces();
            ChessBoard.clearThreaten();
            ChessBoard.setThreatenNoKing();
            ChessBoard.setThreatenKing();
            if (Player.m_whiteTurn && ChessBoard.WhiteInCheck || !Player.m_whiteTurn && ChessBoard.BlackInCheck) {
                Debug.Log("illegal move - move puts player in check");
                transform.parent = _startParent;
                transform.position = new Vector3(_startPosition.x, transform.position.y, _startPosition.z);
                StartCoroutine(dropObject());
                Player.m_holding = null;
                return true;
            }
        }
        return false;
    }

    // REQUEST FOR MOVE VALIDATION - handles move attempt OUT of check
    private bool isIllegalMoveFromCheck() {
        if (Player.m_whiteTurn && ChessBoard.WhiteInCheck || !Player.m_whiteTurn && ChessBoard.BlackInCheck) {
            if (transform.parent.childCount > 1) {
                killAndStore();
                ChessBoard.updateLivePieces();
                ChessBoard.clearThreaten();
                ChessBoard.setThreatenNoKing();
                ChessBoard.setThreatenKing();
                if (Player.m_whiteTurn && ChessBoard.WhiteInCheck || !Player.m_whiteTurn && ChessBoard.BlackInCheck) {
                    Debug.Log("illegal move out of check - take results in check");
                    Undo.PerformUndo();
                    transform.parent = _startParent;
                    transform.position = new Vector3(_startPosition.x, transform.position.y, _startPosition.z);
                    StartCoroutine(dropObject());
                    Player.m_holding = null;
                    return true;
                }
            } else {
                ChessBoard.updateLivePieces();
                ChessBoard.clearThreaten();
                ChessBoard.setThreatenNoKing();
                ChessBoard.setThreatenKing();
                if (Player.m_whiteTurn && ChessBoard.WhiteInCheck || !Player.m_whiteTurn && ChessBoard.BlackInCheck) {
                    Debug.Log("illegal move out of check - move keeps player in check");
                    transform.parent = _startParent;
                    transform.position = new Vector3(_startPosition.x, transform.position.y, _startPosition.z);
                    StartCoroutine(dropObject());
                    Player.m_holding = null;
                    return true;
                }
            }
        }
        return false;
    }

    private void killAndStore() {
        bool color = Player.m_holding.GetComponent<ChessPiece>().isWhite;
        // destroy the piece opposite to color
        ChessPiece[] both = transform.parent.GetComponentsInChildren<ChessPiece>();
        foreach (ChessPiece _b in both) {
            if (_b.isWhite != color) {
                Undo.DestroyObjectImmediate(_b.gameObject);
                return;
            }
        }
    }

    // returns true if a double step was completed, false otherwise
    private bool doubleStep() {
        int r_ = 0; int c_ = 0;
        if (gameObject.GetComponent<Pawn>().isWhite) {
            // check if the pawn is in row index 3 (0-based index)
            ChessBoard.getLocation(gameObject.transform.parent.GetComponent<BoardSpace>(), ref r_, ref c_);
            if (r_ == 3) return true;
            return false;
        }
        // check if pawn is in row index 4 (0-based index)
        ChessBoard.getLocation(gameObject.transform.parent.GetComponent<BoardSpace>(), ref r_, ref c_);
        if (r_ == 4) return true;
        return false;
    }

    // looks in adjacent columns for enemy pawn - if found, call setEnpassant on that pawn
    // pass coordinates for the capture space for the enemy pawn (i.e. THIS pawn's location 
    // -1 row if THIS pawn is white, +1 row if THIS pawn is black
    private void checkForEnpassant() {
        int r_ = 0; int c_ = 0;
        ChessBoard.getLocation(gameObject.transform.parent.GetComponent<BoardSpace>(), ref r_, ref c_);
        bool isWhiteThis = gameObject.GetComponent<ChessPiece>().isWhite;
        // if inbound, has a piece, piece is not friendly
        if (ChessUtility.inBound(r_, c_ + 1) && 
            ChessBoard.space[r_, c_ + 1].hasChessPiece() &&
            !ChessBoard.space[r_, c_ + 1].hasFriendlyPiece()) {
            Debug.Log("inbound, has piece, piece not friendly");
            // if piece is a pawn - know it must be enemy pawn
            if (ChessBoard.space[r_, c_ + 1].hasPawnPiece()) {
                if (isWhiteThis) {
                    ChessBoard.space[r_, c_ + 1].transform.GetChild(0).gameObject.GetComponent<Pawn>().setEnpassant(r_ - 1, c_);
                } else {
                    ChessBoard.space[r_, c_ + 1].transform.GetChild(0).gameObject.GetComponent<Pawn>().setEnpassant(r_ + 1, c_);
                }
            }

        }
        if (ChessUtility.inBound(r_, c_ - 1) &&
            ChessBoard.space[r_, c_ - 1].hasChessPiece() &&
            !ChessBoard.space[r_, c_ - 1].hasFriendlyPiece()) {
            Debug.Log("inbound, has piece, piece not friendly");
            // if piece is a pawn - know it must be enemy pawn
            if (ChessBoard.space[r_, c_ - 1].hasPawnPiece()) {
                if (isWhiteThis) {
                    ChessBoard.space[r_, c_ - 1].transform.GetChild(0).gameObject.GetComponent<Pawn>().setEnpassant(r_ - 1, c_);
                } else {
                    ChessBoard.space[r_, c_ - 1].transform.GetChild(0).gameObject.GetComponent<Pawn>().setEnpassant(r_ + 1, c_);
                }
            }

        }
    }

    private void checkPawnSpecialCase() {
        // check if piece moved is a pawn
        if (gameObject.GetComponent<Pawn>()) {
            // if was the first move AND was a double step
            if (!gameObject.GetComponent<Pawn>().firstMoveComplete && doubleStep()) {
                Debug.Log("first move AND was a double step");
                checkForEnpassant(); // checks and possibly activates flag for enemy pawn
            }
            // if enpassant was active for this pawn, deactivate it
            if (gameObject.GetComponent<Pawn>().canTakeEnpassant) {
                // if enpassant maneuver was completed, destroy the enemy piece
                if (gameObject.GetComponent<Pawn>().getEnpassant()) {
                    killEnemyPieceEnpassant();
                }
            }
            // ensure this flag gets set
            gameObject.GetComponent<Pawn>().firstMoveComplete = true;
        }
        // disable enpassant for any pieces of the holding type
        Component[] pawns;
        pawns = ChessBoard.Chessboard.GetComponentsInChildren(typeof(Pawn));
        if (pawns != null) {
            foreach (Pawn p in pawns) {
                // if white's turn, disable all enpassant for white pawns
                if (Player.m_whiteTurn && p.isWhite) { p.disableEnpassant(); } 
                // if black's turn, disable all enpassant for black pawns
                else if (!Player.m_whiteTurn && !p.isWhite) { p.disableEnpassant(); }
            }
        }
    }

    private void killEnemyPieceEnpassant() {
        int r_ = 0; int c_ = 0;
        bool isWhiteThis = Player.m_holding.GetComponent<ChessPiece>().isWhite;
        if (isWhiteThis) {
            // if white, kill piece at -1 row
            ChessBoard.getLocation(transform.parent.GetComponent<BoardSpace>(), ref r_, ref c_);
            Destroy(ChessBoard.space[r_ - 1, c_].transform.GetChild(0).gameObject);
        } else {
            // if black, kill piece at +1 row
            ChessBoard.getLocation(transform.parent.GetComponent<BoardSpace>(), ref r_, ref c_);
            Destroy(ChessBoard.space[r_ + 1, c_].transform.GetChild(0).gameObject);
        }
    }

    private void killEnemyPiece () {
        bool color = Player.m_holding.GetComponent<ChessPiece>().isWhite;
        // destroy the piece opposite to color
        ChessPiece[] both = transform.parent.GetComponentsInChildren<ChessPiece>();
        foreach (ChessPiece _b in both) {
            if (_b.isWhite != color) {
                Destroy(_b.gameObject);
                Debug.Log("piece destroyed");
                return;
            }
        }
    }

}
