using UnityEngine;
using System.Collections;

namespace chess {

    struct location {
        public int R;
        public int C;
    }

    public enum piece {
        PAWN = 0,
        ROOK = 1,
        BISHOP = 2,
        KNIGHT = 3,
        KING = 4,
        QUEEN = 5
    }

    public enum col {
        A = 0,
        B = 1,
        C = 2,
        D = 3,
        E = 4,
        F = 5, 
        G = 6,
        H = 7,
        TOTAL = 8
    }

    public enum mat {
        BLACK,
        WHITE,
        HIGHLIGHT_VALID,
        HIGHLIGHT_FRIENDLY,
        HIGHLIGHT_ENEMY
    }

    public static class ChessUtility {

        public static int MAX_ROWS = 8;
        public static int MAX_COLS = 8;
        public static int NUMBER_OF_PIECES = 6;
        public static int NUMBER_OF_MATERIALS = 5;

        public static string[] MaterialFile = {
            "ChessBlack",
            "ChessWhite",
            "ChessHighlightValid",
            "ChessHighlightFriendly",
            "ChessHighlightEnemy"
        };

        public static string[] BlackPieceFile = {
            "black_pawn_empty",
            "black_rook_empty",
            "black_bishop_empty",
            "black_knight_empty",
            "black_king_empty",
            "black_queen_empty"
        };

        public static string[] WhitePieceFile = {
            "white_pawn_empty",
            "white_rook_empty",
            "white_bishop_empty",
            "white_knight_empty",
            "white_king_empty",
            "white_queen_empty"
        };

        // takes destination coordinates, returns true if in bound, false otherwise
        public static bool inBound(int r_, int c_) {
            if (r_ >= 0 && r_ < MAX_ROWS && c_ >= 0 && c_ < MAX_COLS) {
                return true;
            }
            return false;
        }

    } 
}
