using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;

[RequireComponent(typeof(BoardSpace))]
public class TriggerHandlerSpace : MonoBehaviour {
    void OnTriggerEnter (Collider other) {
        // friendly colliders are not enabled, so if the childCount is > 0 it must be an enemy
        //if (transform.childCount > 0) {
            // destroy the object in that location
            //Destroy(transform.GetChild(0).gameObject);
          //  return;
        //}


        // set parent to this object
        other.gameObject.transform.parent = transform;
    }
}
