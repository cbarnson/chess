using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Menu : MonoBehaviour {

    public Button _button;
    private Fade _fade;

    void Awake () {
        _fade = GetComponent<Fade>();
        CanvasGroup _group = GetComponent<CanvasGroup>();
        _group.alpha = 0.0f;
        _group.interactable = false;
        _group.blocksRaycasts = false;
    }

    public void openMenu() {
        _fade.FadeMeIn();
        _button.interactable = false;
    }

    public void closeMenu() {
        _fade.FadeMeOut();
        _button.interactable = true;
    }

}
