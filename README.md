# CHESS

3D Chess made in Unity. Developed by Cody Barnson as a solo project

## Installation

TODO: Describe the installation process

## To Be Implemented
- castling
- pawn promotion
- stalemate
- double check
- 50 move draw
- repetition of moves rules

## Coming Soon
- turn clock
- menu interface

## Usage

TODO: Write usage instructions

## License

    3D Chess - classic
    Copyright (C) 2016 Cody Barnson

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.